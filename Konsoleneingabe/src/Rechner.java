import java.util.Scanner; // Import der Klasse Scanner

public class Rechner {

	public static void main(String[] args) // Hier startet das Programm
	{

		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);

		boolean exit = true;
		while (exit) {
			System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

			// Die Variable zahl1 speichert die erste Eingabe
			int zahl1 = myScanner.nextInt();

			System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

			// Die Variable zahl2 speichert die zweite Eingabe
			int zahl2 = myScanner.nextInt();
			System.out.println("Bitte Geben Sie ein Rechenzeichen ein (+,-,*,/,(e)xit");
			switch (myScanner.next().charAt(0)) {
			case '+':
				// Die Addition der Variablen zahl1 und zahl2
				// wird der Variable ergebnis zugewiesen.
				int ergebnis = zahl1 + zahl2;

				System.out.print("\n\n\nErgebnis der Rechnung lautet: ");
				System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);
				break;
			case '-':
				// Die Subtrakion der Variablen zahl1 und zahl2
				// wird der Variable ergebnis zugewiesen.
				ergebnis = zahl1 - zahl2;

				System.out.print("\n\n\nErgebnis der Rechnung lautet: ");
				System.out.println(zahl1 + " - " + zahl2 + " = " + ergebnis);
				break;

			case '*':
				// Die Multiplikation der Variablen zahl1 und zahl2
				// wird der Variable ergebnis zugewiesen.
				ergebnis = zahl1 * zahl2;

				System.out.print("\n\n\nErgebnis der Rechnung lautet: ");
				System.out.println(zahl1 + " * " + zahl2 + " = " + ergebnis);
				
				break;

			case '/':
				// Die Dividende der Variablen zahl1 und zahl2
				// wird der Variable ergebnis zugewiesen.
				ergebnis = zahl1 / zahl2;

				System.out.print("\n\n\nErgebnis der Rechnung lautet: ");
				System.out.println(zahl1 + " / " + zahl2 + " = " + ergebnis);
				break;
				
			case 'e':
				exit = false;
				break;
			default:
			System.out.println("Sie haben ein ungültiges Zeichen eingegeben");	
			}
		
		}

		myScanner.close();

	}
}
