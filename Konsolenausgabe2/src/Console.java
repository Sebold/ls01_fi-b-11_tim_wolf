
public class Console {
public static void main(String args[]) {
// AUFGABE 1 //
//	System.out.println("    **   ");
//	System.out.println(" *      *");
//	System.out.println(" *      *");
//	System.out.println("    **   ");
	
// AUFGABE 2 //	
//int n = 0;	
//	System.out.printf( "%-5s", n + "!");
//	System.out.print("= ");
//	System.out.printf( "%-19s", "" );
//	System.out.print("=");
//	System.out.printf( "%4s\n", n+1	 );
//	
//	
//	System.out.printf( "%-5s", (n+1) + "!");
//	System.out.print("= ");
//	System.out.printf( "%-19s", n+1 );
//	System.out.print("=");
//	System.out.printf( "%4s\n", n	 );
//	
//	
//	System.out.printf( "%-5s", (n+2) + "!");
//	System.out.print("= ");
//	System.out.printf( "%-19s", "1 * 2" );
//	System.out.print("=");
//	System.out.printf( "%4s\n", (n+1) * (n+2)	 );
//	
//	
//	System.out.printf( "%-5s", (n+3) + "!" );
//	System.out.print("= ");
//	System.out.printf( "%-19s", "1 * 2 * 3" );
//	System.out.print("=");
//	System.out.printf( "%4s\n", (n+1)*(n+2)*(n+3)	 );
//	
//	
//	System.out.printf( "%-5s", (n+4) + "!");
//	System.out.print("= ");
//	System.out.printf( "%-19s", "1 * 2 * 3 * 4" );
//	System.out.print("=");
//	System.out.printf( "%4s\n", (n+1)*(n+2)*(n+3)*(n+4)	 );
//	
//	
//	System.out.printf( "%-5s", (n+5) + "!");
//	System.out.print("= ");
//	System.out.printf( "%-19s", "1 * 2 * 3 * 4 * 5" );
//	System.out.print("=");
//	System.out.printf( "%4s\n", (n+1)*(n+2)*(n+3)*(n+4)*(n+5) );
	
// AUFGABE 3 //

	double n1 = -28.8889;
	double n2 = -23.3333;
	double n3 = -17.7778;
	double n4 = -6.6667;
	double n5 = -1.1111;
	System.out.printf( "%-12s", "Fahrenheit");
	System.out.print("|");
	System.out.printf("%10s\n", "Celsius");
	
	System.out.println("-----------------------");

	System.out.printf( "%-12s", "-20");
	System.out.print("|");
	
	System.out.printf("%10.2f\n", n1);
	
	System.out.printf( "%-12s", "-10");
	System.out.print("|");
	System.out.printf("%10.2f\n", n2);
	
	System.out.printf( "%-12s", "0");
	System.out.print("|");
	System.out.printf("%10.2f\n", n3);
	
	System.out.printf( "%-12s", "20");
	System.out.print("|");
	System.out.printf("%10.2f\n", n4);
	
	System.out.printf( "%-12s", "30");
	System.out.print("|");
	System.out.printf("%10.2f\n", n5);
}

}
