import java.util.Random;
import java.util.LinkedList;
import java.util.List;

public class Twist {
	
	Random r = new Random();

	public Twist() {												// leerer Konstruktor zur Objekterschaffung
		super();

	}

	public List<String> divideString(String input) {				// Methode spaltet einen String in eine Liste mit den einzelnen Buchstaben
		List<String> dividedlist = new LinkedList<String>();			// um diese dann sp�ter twisten zu k�nnen
		for (int i = 0; i < (input.length()); i++) {
			String filler = "";										// Ein leerer String wird hier verwendet, weil eine Liste mit dem "String" Datentyp
			filler = filler + input.charAt(i);						// keine chars aufnehmen kann. So umgehen wir diesen Fehler
			dividedlist.add(filler);
		}
		return dividedlist;
	}

	public List<String> twistWord(List<String> input) {

		List<String> temp = new LinkedList<String>();
		int count = (input.size() - 1);								// Z�hler wird hier initialisiert, damit die Gr��e statisch bleibt.
		for (int i = 1; i < count; i++) {							// die Schleife geht die gesamt Liste durch
			temp.add(input.get(1));									// temp�r�re Liste nimmt sich den Buchstaben an zweiter stelle
			input.remove(1);										// und entfernt diesen aus der initialen Liste
		}
		while (temp.size() > 0) {									// solange noch Buchstaben in der tempor�ren liste sind soll die Schleife ausgef�hrt werden
			int fill = 0;											// attribut "fill" wird am Beginn der Schleife immer wieder auf 0 gesetzt damit keine Zahlen �ber der gr��e der tempor�ren Liste entstehen k�nnen
						
			fill = r.nextInt(temp.size());							// "fill" wird nun eine zuf�llige Zahl bis maximal der gr��e der aktuell tempor�ren Liste zugewiesen
			
			if (fill == temp.size()) {								// F�r den fall das "fill" eine 0 generiert, um so keine negativen Zahlen erschaffen
				input.add(1, temp.get(fill - 1));					// Der Buchstabe an dem Index welcher zuf�llig durch fill bestimmt wurde wird nun an die 2. Stelle von der initialen Liste eingef�gt
			} else {
				input.add(1, temp.get(fill));						// in diesem Fall ist der index 0, welcher daz� f�hren wurde das eine "negative object function" entstehen w�rde in normalfall
				temp.remove(fill);									// Der Buchstabe wird aus der tempor�ren Liste rausgenommen um keine Duplikate zu erschaffen
			}
		}

	

		return input;
	}

	public String stitchWord(List<String> input) {					// Hier wird die Liste mit dem getwisteten Wort wieder in einen String zusammengesetzt.
		String stitchedword = "";
		for (int i = 0; i < input.size(); i++) {
			stitchedword = stitchedword + input.get(i);
		}

		return stitchedword;
	}
	public void loadingScreen() {
		System.out.println("LOADING");
		System.out.print("[");
		for(int i = 0; i < 10; i++) {
			System.out.print("| ");						// Simpler Ladebalken f�r Visuelle Entspannung, f�r die funktionalit�t nicht ben�tigt
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("]");
		System.out.println("LOADING DONE");
	}
}
