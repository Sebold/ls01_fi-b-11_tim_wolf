import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.omg.Messaging.SyncScopeHelper;

public class DeTwist {
	private List<String> de_words = new LinkedList<String>();
	private String txtfile = "C:\\Users\\WolfTim\\git\\ls01_fi-b-11_tim_wolf\\Twist\\textfiles\\woerterliste.txt";
	Twist t1 = new Twist();

	public void initializeFile() throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(txtfile));

		for (int i = 0; i < 312002; i++) {
			String currentLine = reader.readLine();
			de_words.add(currentLine);
		//	System.out.println(i);

		}
		Collections.sort(de_words);
		//System.out.println("Sorting Done!");
		reader.close();

	}

	public String deTwistword(String input) {
		List<String> templist01 = new LinkedList<String>();
		List<String> templist02 = new LinkedList<String>();
		int loopsize = de_words.size();

		for (int j = 0; j < 3; j++) {
		//	System.out.println(j);

			if (j != 0 && j != 2) {
				loopsize = templist02.size();
				//System.out.println("LOOPSIZE ADJUSTED (tmp02)");
			} else if (j == 2) {
				loopsize = templist01.size();
				//System.out.println("LOOPSIZE ADJUSTED(tmp01)");
			}

			for (int i = 0; i < loopsize; i++) {

				if (j == 0
						&& (t1.divideString(input).get(0).equalsIgnoreCase(t1.divideString(de_words.get(i)).get(0)))) {
					

					templist02.add(de_words.get(i).toString());
				//	System.out.println("INSERTED");

				} else if (j == 0) {
					i = i + 30;
				}

				if (j == 1 && templist02.get(i).charAt(templist02.get(i).length() - 1) == input
						.charAt(input.length() - 1)) {
				//	System.out.println("INDEX = " + i);
					templist01.add(templist02.get(i));
				//	System.out.println("INSERTED R2");
				}

				if (j == 2 && templist01.get(i).length() == input.length()) {
					templist02.add(templist01.get(i));
				//	System.out.println("INSERTED P3");
				}

			}

			if (j == 1) {
				templist02.clear();
				// System.out.println("TEMPLIST02 CLEARED");
			}

			if (j == 2) {
				templist01.clear();
				for (int y = 0; y < templist02.size(); y++) {

					if (this.calculateIndexNr(input) == this.calculateIndexNr(templist02.get(y))) {
						this.calculateIndexNr(input);
						this.calculateIndexNr(templist02.get(y));
						templist01.add(templist02.get(y));

					}
				}

				
			}

		}

		if (templist02.size() == 0) {
			System.out.println(
				"No Word could be deciphered from your Input. Please make sure to write a coherrent german word");
		}

		if (templist01.size() == 0)
			return "No Similar Word Found";
		else if (templist01.size() == 1)
			return "Your Word is: " + templist01.get(0);
		else
		return "Your Word is in this list" + templist01.toString();

	}

	public int calculateIndexNr(String input) {
		int output = 0;
		
		List<String> calcString = t1.divideString(input);

		for (int i = 0; i < calcString.size(); i++) {
			int ascii =(int) calcString.get(i).charAt(0);
			output = output + (ascii * ascii);
			

		}
		return output;
	}

}
