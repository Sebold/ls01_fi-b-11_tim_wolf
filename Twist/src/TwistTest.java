
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.SynchronousQueue;

public class TwistTest {

	public static void main(String args[]) throws IOException {
		Twist t1 = new Twist();
		DeTwist dt1 = new DeTwist();
		Scanner menuInput = new Scanner(System.in);
		Scanner wordInput = new Scanner(System.in);
		String twistword;
		   
		boolean anchor = true;
		dt1.initializeFile();
		
		System.out.println("Twistprogramm 1.243");
		
		while(anchor) {
		System.out.println(" ");
		System.out.println(" ");
		
		System.out.println("Ein Eingegebens Wort Twisten (1)");
		System.out.println("Ein Getwistetes Word entwisten (2)");
		System.out.println("Eine Erkl�rung f�r Twisten (3)");
		System.out.println("Programm beenden (9)");
		
		
		switch(menuInput.next()){
		case("1"):
			System.out.println("Bitte Geben sie ein Wort ein (Beachten sie das W�rter "
			+ "mit 3 Buchstaben zwar angenommen werden, allerdings vom Prinzip her nicht getwisted werden k�nnen)");
			twistword = wordInput.nextLine();
			System.out.println("Ihr Wort wird getwisted");
			t1.loadingScreen();
			System.out.print("Ihr Wort wurde erfolgreich getwisted:  ");
			System.out.println(t1.stitchWord(t1.twistWord(t1.divideString(twistword))));
			break;
		case("2"):
			System.out.println("Bitte geben sie ein getwistetes Wort um es zu enttwisten");
			twistword = wordInput.nextLine();
			System.out.println(dt1.deTwistword(twistword));
			break;
		
		case("3"):
			System.out.println("Der Begriff 'Twisten' stammt vom ehemaligen Pop-S�nger Elvis Presley. Er hatte sich �berlegt wenn ");
			System.out.println("ein Wort bis auf den ersten und letzten Buchstaben zuf�llig gedreht wird, diese Wort nun f�r einen Computer ");
			System.out.println("unlesbar sei. Menschen allerdings k�nnten dieses Wort weiterhin entziffern. Elvis mochte diese Idee um seine ");
			System.out.println("privaten Nachrichten vor Spionage von Maschinen zu sch�tzen.");
			break;
		
		case("9"):
			anchor = false;
			break;
		
		default:
			System.out.println("Es wurde keine g�ltige Eingabe vorgenommen. Alle g�ltigen Eingaben werden in Klammern mit einer Zahl definiert.");
			break;
		}
	}
		 
		
		
		
		
		menuInput.close();
		wordInput.close();

	}

}


