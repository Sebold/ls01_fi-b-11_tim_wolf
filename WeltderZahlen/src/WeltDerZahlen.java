/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Tim Wolf >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 9 ;
    
    // Anzahl der Sterne in unserer Milchstraße
        String anzahlSterne = "280000000000";
    
    // Wie viele Einwohner hat Berlin?
       int bewohnerBerlin = 4700000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
     int  alterTage = 7823;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
      int gewichtKilogramm =  200000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
      int flaecheGroessteLand = 17098242;
    
    // Wie groß ist das kleinste Land der Erde?
    
     double  flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Bewohner in Berlin: " + bewohnerBerlin);
    
    System.out.println("Mein Alter in Tagen: " + alterTage);
    
    System.out.println("Gewicht schwerstes Tier in kg: " + gewichtKilogramm);
    
    System.out.println("Fl�che des gr��ten Landes: " + flaecheGroessteLand);
    
    System.out.println("Fl�che des kleinsten Landes" + flaecheKleinsteLand);
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

