﻿import java.util.InputMismatchException;
import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		

		double zuZahlenderBetrag;
		double rückgabebetrag;
		while(true) {
		zuZahlenderBetrag = fahrkartenbestellungErfassen();

		// Geldeinwurf
		// -----------
		rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

		// Fahrscheinausgabe
		// -----------------
			fahrkartenAusgeben();
		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
			rueckgeldAusgeben(rückgabebetrag);	
	}
		
	}
	public static double fahrkartenbestellungErfassen() {
		double fahrkartenPreis[] = {0,2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
		String fahrkartenBezeichnung[] = {"","Einzefahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
		
		short anzahlTickets = 0;
		double zuZahlenderBetrag = 0;
		double output = 0;
		boolean switchloop = true;
		Scanner betragInput = new Scanner(System.in);
		Scanner anzahlInput = new Scanner(System.in);
		Scanner typeInput = new Scanner(System.in);
		try {
			
		System.out.println("Wählen sie ihre Wunschfahrkarte für Berlin AB aus: ");
		while(switchloop == true) {
		for(int i = 1; i < fahrkartenPreis.length; i++) {
			System.out.println("  " + fahrkartenBezeichnung[i] + " " + "["+ fahrkartenPreis[i] +"€" + "]"+ " (" + i + ") " );
		}
		System.out.println(" ");
		System.out.println("  Geben sie keine Zahl ein um den Bestellvorgang abzuschließen und den Zahlungsvorgang einzuleiten");
//		System.out.println("  Einzelfahrschein Regeltarif AB [3,00 EUR]			(1)");
//		System.out.println("  Tageskarte Regeltarif AB [8,80 EUR]				(2)");
//		System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [25,50 EUR]		(3)");
//		System.out.println("  Bezahlen							(9)");
		System.out.println("                                                       ");
		System.out.println("Anzahl der Tickets: " + anzahlTickets);
			int type = typeInput.nextInt();
			if(type < fahrkartenPreis.length) {
			zuZahlenderBetrag += fahrkartenPreis[type];
			anzahlTickets += 1 ;
			} 
		}
		}
			catch(InputMismatchException e) {
				System.out.println("Sie haben keine Zahl eingegeben. Zahlungsvorgang wird eingeleitet");
			}
		
		
		
		
		
		
		
		 
		
		if(zuZahlenderBetrag < 0) {
			System.out.println("Es wurde ein Ungültiger Preis eingegeben. Es wird mit dem Standardpreis 1 Euro fortgefahren");
			zuZahlenderBetrag = 1;
		}
		
		
		

		output = zuZahlenderBetrag;
		return output;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);
		double output;
		double eingezahlterGesamtbetrag = 0.00;
		while (eingezahlterGesamtbetrag < (zuZahlen)) // zahlender Betrag wird mit der anzahl der Tickets multipliziert
		{
			System.out.println("Noch zu zahlen: ");
			System.out.printf("%.2f", ((zuZahlen) - eingezahlterGesamtbetrag));
			System.out.println(" Euro");
			System.out.print("Eingabe (mind. 5Ct, höchstens 10 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;

		}
		output = eingezahlterGesamtbetrag - zuZahlen;
		
		return output;
	}

	public static void fahrkartenAusgeben() {
	
		System.out.println("\nFahrscheine werden ausgegeben");
		warte(250);
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von ");
			System.out.printf("%.2f", rückgabebetrag);
			System.out.println(" EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2,"EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1,"EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50,"CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20,"CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10,"CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5,"CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, die Fahrscheine\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
		try {
			Thread.sleep(3500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("                                                       ");
		System.out.println("                                                       ");
		System.out.println("                                                       ");
		System.out.println("                                                       ");
	}
	
	public static void warte(int millisec) {
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(millisec);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

}